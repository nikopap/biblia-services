<?php

namespace Webnestors\BibliaServices;

class Settings {
	const option_name = "biblia-services_options";

	public string $osdelnet_endpoint_url = "";
	public string $osdelnet_api_user = "";
	public string $osdelnet_api_pass = "";
	public string $jet_engine_cct_name = "";
	public string $submit_redirect_path = "";
	public string $placeholder_image_url = "";
	public string $login_url = "";

	/**
	 * Preload option data only when needed
	 */
	function preload(): void {
		$options = get_option(self::option_name);

		if ($options) {
			foreach ($options as $key => $value) {
				if (property_exists($this, $key)) {
					$this->{$key} = $value;
				}
			}
		}
	}

	function register_menu(): void {
		add_options_page(
			"Biblia Services",
			"Biblia Services",
			"manage_options",
			"biblia-services",
			[$this, "render_settings_page"]
		);
	}

	function register_settings(): void {
		register_setting("biblia-services", self::option_name, [
			"type" => "array",
			"show_in_rest" => false
		]);

		add_settings_section(
			"biblia-services-api",
			"API Settings",
			function () {},
			"biblia-services"
		);

		add_settings_section(
			"biblia-services",
			"General Settings",
			function () {},
			"biblia-services"
		);

		add_settings_field(
			"osdelnet_endpoint_url",
			"Osdelnet API URL",
			function () {
				echo '<input type="text" class="regular-text" ' .
					$this->common_option_attrs("osdelnet_endpoint_url") .
					" />";
			},
			"biblia-services",
			"biblia-services-api"
		);

		add_settings_field(
			"osdelnet_api_user",
			"Osdelnet API User",
			function () {
				echo '<input type="password" class="regular-text" ' .
					$this->common_option_attrs("osdelnet_api_user") .
					" />";
			},
			"biblia-services",
			"biblia-services-api"
		);

		add_settings_field(
			"osdelnet_api_pass",
			"Osdelnet API Password",
			function () {
				echo '<input type="password" class="regular-text" ' .
					$this->common_option_attrs("osdelnet_api_pass") .
					" />";
			},
			"biblia-services",
			"biblia-services-api"
		);

		add_settings_field(
			"jet_engine_cct_name",
			"JetEngine CCT Name",
			function () {
				echo '<input type="text" class="regular-text" ' .
					$this->common_option_attrs("jet_engine_cct_name") .
					" />";
			},
			"biblia-services",
			"biblia-services"
		);

		add_settings_field(
			"submit_redirect_path",
			__("Redirect Path", "biblia-services"),
			function () {
				echo '<input type="text" class="regular-text" ' .
					$this->common_option_attrs("submit_redirect_path") .
					" />";
				echo "<p>" .
					__("Where to redirect when the user selects a new book", "biblia-services") .
					"</p>";
				echo "<p><code>/add-book/</code></p>";
			},
			"biblia-services",
			"biblia-services"
		);

		add_settings_field(
			"login_url",
			__("Login URL", "biblia-services"),
			function () {
				echo '<input type="text" class="regular-text" ' .
					$this->common_option_attrs("login_url") .
					" />";
			},
			"biblia-services",
			"biblia-services"
		);

		add_settings_field(
			"placeholder_image_url",
			__("Placeholder Image URL", "biblia-services"),
			function () {
				echo '<input type="text" class="regular-text" ' .
					$this->common_option_attrs("placeholder_image_url") .
					" />";
			},
			"biblia-services",
			"biblia-services"
		);
	}

	function render_settings_page(): void {
		$this->preload(); ?>
		<div class="wrap">
			<h1>Settings</h1>

			<form action="options.php" method="post">
			<?php
   settings_fields("biblia-services");
   do_settings_sections("biblia-services");
   submit_button();?>
			</form>
		</div>
		<?php
	}

	private function common_option_attrs(string $key): string {
		return 'name="' . self::option_name . "[" . $key . ']" value="' . $this->{$key} . '"';
	}
}
