<?php

namespace Webnestors\BibliaServices;

final class View {
	private string $template;
	private array $scripts;
	private array $styles;

	function __construct(string $name, array $scripts = [], array $styles = []) {
		$this->template = dirname(__FILE__) . "/../includes/$name.php";
		$this->scripts = $scripts;
		$this->styles = $styles;
	}

	/**
	 * @param ?object $context
	 */
	function __invoke($context): string {
		if (!file_exists(realpath($this->template))) {
			throw new \Exception("Required file '{$this->template}' does not exist");
		}

		foreach ($this->scripts as $script) {
			wp_enqueue_script($script);
		}

		foreach ($this->styles as $style) {
			wp_enqueue_style($style);
		}

		$callback = function (string $template): string {
			$vars = get_object_vars($this);
			extract($vars);
			ob_start();
			/** @psalm-suppress UnresolvableInclude */
			include $template;
			return ob_get_clean();
		};

		if (is_object($context)) {
			$func = $callback->bindTo($context);

			if ($func === null) {
				throw new \Exception("Failed to bind context to Closure");
			}
		} else {
			$func = $callback;
		}

		return call_user_func($func, $this->template);
	}
}
