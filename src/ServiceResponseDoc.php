<?php

namespace Webnestors\BibliaServices;

class ServiceResponseDoc {
	function __construct(
		public readonly ?string $distinctive_title,
		public readonly ?string $distinctive_subtitle,
		public readonly ?string $original_title,
		public readonly ?string $original_subtitle,
		public readonly ?string $pub_id,
		public readonly ?string $pub_name,
		public readonly ?int $pub_month,
		public readonly ?int $pub_year,
		public readonly ?int $pub_day,
		public readonly array $authors,
		public readonly ?string $prefix,
		public readonly ?string $isbn13,
		public readonly ?float $price,
		public readonly ?string $description,
		public readonly ?int $pages,
		public readonly ?string $cover_img_url
	) {
	}
}
