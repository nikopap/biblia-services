function toggleLoadingButton(target) {
  target.prop("disabled", target.prop("disabled") ? false : true);
  jQuery(target).find(".button-content").toggle();
  jQuery(target).find(".button-spinner").toggle();
}

function setSubmitError(target, msg) {
  jQuery(target)
    .closest(".biblia-services-result-wrapper")
    .find(".biblia-services-submit-error")
    .text(msg);
}

function submitBookHandler(event) {
  event.preventDefault();

  const data = Object.fromEntries(new FormData(event.target));
  const submitButton = jQuery(event.target).find("button[type=submit]");

  toggleLoadingButton(submitButton);

  wp.apiRequest({
    path: "/biblia-services/new-cover",
    type: "POST",
    data: {
      isbn: data?.isbn_13,
    }
  }).fail(function(xhr) {
    toggleLoadingButton(submitButton);

    if (xhr.status === 401) {
      // Unauthenticated, must log in
      setSubmitError(event.target, "You must be logged in.")
      window.location = event.target.dataset.login_url || "/wp-login.php";
    } else {
      setSubmitError(
        event.target,
        "There was a problem submitting your request. Please try again later."
      );
    }
  }).done(function(coverResult) {
    wp.apiRequest({
      path: `/jet-cct/${event.target.dataset.cct}`,
      type: "POST",
      data: { ...data, book_cover: coverResult.id }
    }).always(function() {
      toggleLoadingButton(submitButton);
    }).fail(function() {
      setSubmitError(
        event.target,
        "There was a problem submitting your request. Please try again later."
      );
    }).done(function(createResult) {
      window.location.replace(`${event.target.dataset.redirect}?item_id=${createResult.item_id}`);
    });
  });
}

document.addEventListener("DOMContentLoaded", function() {
  if (!wp.apiRequest) {
    console.error("biblia-services: missing required dependency `wp.apiRequest`");
    return;
  }

  const resultsTarget = document.getElementById("biblia-services-results");
  const submitButton = jQuery("#biblia-services-search-form button[type=submit]");
  submitButton.find(".button-spinner").hide();

  jQuery("#biblia-services-search-form").on("submit", function(e) {
    e.preventDefault();
    const data = Object.fromEntries(new FormData(e.target));

    wp.apiRequest({
      path: `/biblia-services/book-search`,
      type: "GET",
      data,
      beforeSend: function() {
        toggleLoadingButton(submitButton);
      },
    }).done(function(data) {
      resultsTarget.innerHTML = data?.html;
      jQuery("#biblia-services-search-form .search-input").removeClass("error");
      jQuery(".biblia-services-submit-form .button-spinner").hide();
      jQuery(".biblia-services-submit-form").on("submit", submitBookHandler);
    }).fail(function(xhr) {
      jQuery("#biblia-services-search-form .search-input").addClass("error");
      resultsTarget.innerText = xhr.responseJSON.message;
    }).always(function() {
      toggleLoadingButton(submitButton);
    });
  });
});
