<div class="biblia-services-result-wrapper">
	<div class="biblia-services-result">
		<div class="biblia-services-result-cover">
			<?php if ($cached_image_url): ?>
				<img src="<?php echo wp_kses_post($cached_image_url); ?>" />
			<?php elseif (empty($doc->cover_img_url)): ?>
				<img src="<?php echo wp_kses_post($placeholder_image_url); ?>" />
			<?php else: ?>
				<img src="<?php echo wp_kses_post($doc->cover_img_url); ?>" />
			<?php endif; ?>
		</div>
		<div class="biblia-services-result-main">
			<div>
				<div class="book-title">
					<?php echo wp_kses_post($doc->distinctive_title); ?>
				</div>
				<?php if (!empty($doc->distinctive_subtitle)): ?>
					<div class="book-distinctive-title">
						<?php echo wp_kses_post($doc->distinctive_subtitle); ?>
					</div>
				<?php endif; ?>
			</div>
			<div>
				<?php if (!empty($doc->original_title)): ?>
					<div class="book-original-title">
						<?php echo wp_kses_post($doc->original_title); ?>
					</div>
				<?php endif; ?>
				<?php if (!empty($doc->original_subtitle)): ?>
					<div class="book-original-subtitle">
						<?php echo wp_kses_post($doc->original_subtitle); ?>
					</div>
				<?php endif; ?>
				<?php if (count($doc->authors)): ?>
					<div class="book-author">
						<?php echo wp_kses_post(implode(", ", $doc->authors)); ?>
					</div>
				<?php endif; ?>
				<?php if (!empty($doc->isbn13)): ?>
					<div class="book-isbn">
						<?php echo wp_kses_post($doc->isbn13); ?>
					</div>
				<?php endif; ?>
				<?php if (!empty($doc->pub_name)): ?>
					<div class="book-pub-name">
						<?php echo wp_kses_post($doc->pub_name); ?>
					</div>
				<?php endif; ?>
				<?php if (!empty($doc->pub_year)): ?>
					<div class="book-pub-year">
						<?php echo wp_kses_post($doc->pub_year); ?>
					</div>
				<?php endif; ?>
			</div>
		</div>
		<form
			method="POST"
			class="biblia-services-submit-form"
			data-cct="<?php echo esc_attr($cct_name); ?>"
			data-redirect="<?php echo esc_attr($redirect); ?>"
			data-login_url="<?php echo esc_attr($login_url); ?>"
		>
			<input type="hidden" name="book_title" value="<?php echo esc_attr($doc->distinctive_title); ?>" />
			<input
				type="hidden"
				name="book_subtitle"
				value="<?php echo esc_attr($doc->distinctive_subtitle); ?>"
			/>
			<input type="hidden" name="isbn_13" value="<?php echo esc_attr($doc->isbn13); ?>" />
			<input
				type="hidden"
				name="book_description"
				value="<?php echo esc_attr($doc->description); ?>"
			/>
			<input
				type="hidden"
				name="book_author"
				value="<?php echo esc_attr(implode(", ", $doc->authors)); ?>"
			/>
			<input
				type="hidden"
				name="published_year"
				value="<?php echo esc_attr($doc->pub_year); ?>"
			/>
			<input
				type="hidden"
				name="bookstore_price"
				value="<?php echo esc_attr($doc->price); ?>"
			/>
			<input
				type="hidden"
				name="book_pages"
				value="<?php echo esc_attr($doc->pages); ?>"
			/>
			<input
				type="hidden"
				name="publication_by"
				value="<?php echo esc_attr($doc->pub_name); ?>"
			/>
			<button class="biblia-services-button" type="submit">
				<span class="button-content"><?php _e("Select"); ?></span>
				<span class="button-spinner"></span>
			</button>
		</form>
	</div>
	<div class="biblia-services-submit-error"></div>
</div>
