<?php

namespace Webnestors\BibliaServices;

class ServiceResponse {
	/** @param ServiceResponseDoc[] $docs */
	function __construct(public readonly int $count, public readonly array $docs) {
	}
}
