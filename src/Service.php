<?php

namespace Webnestors\BibliaServices;

final class Service {
	// http://solr.osdelnet.gr/solr/index.php?QUERYSTRING
	function __construct(private string $url, private string $user, private string $pass) {
	}

	/**
	 * @return ServiceResponse|false
	 */
	function search_by_title(string $title) {
		$query = "title_search:($title)";

		$response_or_error = $this->call($query);

		if (is_wp_error($response_or_error)) {
			//debug_log(
			//	"[Biblia Services]: Failed to retrieve data from API: " .
			//		$response_or_error->get_error_message()
			//);

			return false;
		}

		return $response_or_error;
	}

	/**
	 * @param string[] $columns
	 *
	 * @return ServiceResponse|false
	 */
	function search_by_isbn(string $isbn, array $columns = []) {
		$query = "isbn13_search:($isbn)";

		$response_or_error = $this->call($query, $columns);

		if (is_wp_error($response_or_error)) {
			//debug_log(
			//	"[Biblia Services]: Failed to retrieve data from API: " .
			//		$response_or_error->get_error_message()
			//);

			return false;
		}

		return $response_or_error;
	}

	/**
	 * @return ServiceResponse|false
	 */
	function search_by_title_or_isbn(string $title_or_isbn) {
		$query = "title_search:($title_or_isbn) OR isbn13_search:($title_or_isbn)";

		$response_or_error = $this->call($query);

		if (is_wp_error($response_or_error)) {
			//debug_log(
			//	"[Biblia Services]: Failed to retrieve data from API: " .
			//		$response_or_error->get_error_message()
			//);

			return false;
		}

		return $response_or_error;
	}

	/**
	 * @param string[] $columns
	 *
	 * @return ServiceResponse|\WP_Error
	 */
	private function call(string $query, array $columns = []) {
		$url = esc_url_raw($this->get_query_url($query, $columns));
		$response = wp_remote_get($url);

		if (is_wp_error($response)) {
			return $response;
		}

		if (200 !== ($code = wp_remote_retrieve_response_code($response))) {
			return new \WP_Error(
				"biblia-services",
				"Remote API responded with status code " . $code
			);
		}

		$body = json_decode(wp_remote_retrieve_body($response), true);

		if (!isset($body["response"]) || !isset($body["response"]["docs"])) {
			return new \WP_Error("biblia-services", "Invalid response body from remote", [
				"response_was" => $body,
				"called_url" => $url
			]);
		}

		$docs = [];

		foreach ($body["response"]["docs"] as $doc) {
			$authors = [];
			$roles = $doc["contr_role"] ?? [];

			foreach ($roles as $idx => $role) {
				if ($role == "Συγγραφέας") {
					$authors[] = $doc["contr_name"][$idx];
				}
			}

			$docs[] = new ServiceResponseDoc(
				distinctive_title: $doc["distinctive_title"] ?? null,
				distinctive_subtitle: $doc["distinctive_subtitle"] ?? null,
				original_title: $doc["original_title"] ?? null,
				original_subtitle: $doc["original_subtitle"] ?? null,
				pub_id: $doc["pub_id"] ?? null,
				pub_name: $doc["pub_name"] ?? null,
				pub_month: $doc["pub_month"] ?? null,
				pub_year: $doc["pub_year"] ?? null,
				pub_day: $doc["pub_day"] ?? null,
				prefix: $doc["prefix"] ?? null,
				isbn13: $doc["isbn13"] ?? null,
				price: $doc["price"] ?? null,
				description: $doc["description"] ?? null,
				pages: $doc["pages"] ?? null,
				cover_img_url: $doc["cover"] ?? null,
				authors: $authors
			);
		}

		$service_response = new ServiceResponse(
			count: $body["response"]["numFound"] ?? 0,
			docs: $docs
		);

		return $service_response;
	}

	/**
	 * @param string[] $columns
	 */
	private function get_query_url(string $query, array $columns = []): string {
		$data = [
			"q" => $query,
			"wt" => "json",
			"user" => $this->user,
			"pass" => $this->pass
		];

		if (count($columns)) {
			$data["fl"] = implode(",", $columns);
		}

		return $this->url . "?" . http_build_query($data);
	}
}
