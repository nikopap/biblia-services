<form
	class="search-bar"
	id="biblia-services-search-form"
	method="POST"
	action="<?php echo admin_url("admin-ajax.php"); ?>"
>
	<input
		class="search-input"
		name="title_or_isbn"
		type="text"
		placeholder="<?php esc_attr_e("Search by title or isbn", "biblia-services"); ?>"
	/>
	<input type="hidden" name="action" value="biblia-services:search" />
	<button class="biblia-services-button biblia-services-button-search" type="submit">
		<span class="button-content"><?php _e("Search"); ?></span>
		<span class="button-spinner"></span>
	</button>
</form>
<div id="biblia-services-results"></div>
