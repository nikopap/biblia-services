<?php

namespace Webnestors\BibliaServices;

class API_Controller extends \WP_REST_Controller {
	public function __construct(public Settings $settings) {
		$this->namespace = "biblia-services";
		$this->rest_base = "";
		$this->schema = [];
	}

	public function register_routes(): void {
		register_rest_route($this->namespace, "book-search", [
			"methods" => \WP_REST_Server::READABLE,
			"callback" => [$this, "book_search"],
			"permission_callback" => [$this, "book_search_permissions"]
		]);

		register_rest_route($this->namespace, "new-cover", [
			"methods" => \WP_REST_Server::CREATABLE,
			"callback" => [$this, "new_cover"],
			"permission_callback" => [$this, "new_cover_permissions"]
		]);
	}

	/** @return \WP_Error|\WP_REST_Response */
	public function book_search(\WP_REST_Request $request) {
		$title_or_isbn = $request->get_param("title_or_isbn");

		if (!$title_or_isbn || mb_strlen(trim($title_or_isbn)) <= 2) {
			return new \WP_Error(
				"biblia-services",
				__("Search must be more than 2 characters long.", "biblia-services"),
				["status" => 400]
			);
		}

		$this->settings->preload();

		$service = new Service(
			url: $this->settings->osdelnet_endpoint_url,
			user: $this->settings->osdelnet_api_user,
			pass: $this->settings->osdelnet_api_pass
		);

		$service_response = $service->search_by_title_or_isbn(trim($title_or_isbn));

		if (!$service_response) {
			return new \WP_Error(
				"biblia-services",
				__(
					"There was an internal service error. Please try again later.",
					"biblia-services"
				),
				["status" => 500]
			);
		}

		if (count($service_response->docs) == 0) {
			return new \WP_Error(
				"biblia-services",
				__("We couldn't find any books with your query.", "biblia-services"),
				["status" => 404]
			);
		}

		$result_view = new View("result");
		$html = "";

		foreach ($service_response->docs as $doc) {
			$cached_image_url = null;

			/** @var \WP_Post[] */
			$attachments = get_posts([
				"title" => $doc->isbn13,
				"post_type" => "attachment"
			]);

			if (count($attachments)) {
				$cached_image_url = wp_get_attachment_image_url($attachments[0]->ID, "full");
				$cached_image_url = $cached_image_url ? $cached_image_url : null;
			}

			$context = new class (
				$this->settings->submit_redirect_path,
				$this->settings->jet_engine_cct_name,
				$this->settings->placeholder_image_url,
				$cached_image_url,
				$doc,
				$this->settings->login_url
			) {
				function __construct(
					public string $redirect,
					public string $cct_name,
					public string $placeholder_image_url,
					public ?string $cached_image_url,
					public ServiceResponseDoc $doc,
					public string $login_url
				) {
				}
			};

			$html .= $result_view($context);
		}

		return new \WP_REST_Response(["html" => $html]);
	}

	public function book_search_permissions(\WP_REST_Request $request): bool {
		return true;
	}

	/** @return \WP_Error|\WP_REST_Response */
	public function new_cover(\WP_REST_Request $request) {
		$isbn = $request->get_param("isbn");

		$internal_error_message = __(
			"There was an internal service error. Please try again later.",
			"biblia-services"
		);

		if (!$isbn || empty(trim($isbn))) {
			return new \WP_Error("biblia-services-internal-1", $internal_error_message, [
				"status" => 500
			]);
		}

		$isbn = trim($isbn);

		/** @var \WP_Post[] */
		$attachments = get_posts([
			"post_type" => "attachment",
			"name" => sanitize_title($isbn),
			"posts_per_page" => 1,
			"post_status" => "inherit"
		]);

		if (count($attachments)) {
			return new \WP_REST_Response(["id" => $attachments[0]->ID]);
		} else {
			// 1. Find the image URL

			$this->settings->preload();

			$service = new Service(
				url: $this->settings->osdelnet_endpoint_url,
				user: $this->settings->osdelnet_api_user,
				pass: $this->settings->osdelnet_api_pass
			);

			$service_response = $service->search_by_isbn($isbn, ["cover"]);

			if (!$service_response) {
				return new \WP_Error("biblia-services-internal-2", $internal_error_message, [
					"status" => 500
				]);
			}

			if (count($service_response->docs) == 0) {
				// This means that we messed up something with the search
				return new \WP_Error("biblia-services-internal-3", $internal_error_message, [
					"status" => 500
				]);
			}

			$img_url = $service_response->docs[0]->cover_img_url;

			if (empty($img_url)) {
				// The book does not have an image which may be normal
				return new \WP_REST_Response(["id" => null, "url" => null]);
			}

			// 2. Download the image file

			if (!function_exists("download_url")) {
				/**
				 * @psalm-suppress UnresolvableInclude
				 * @psalm-suppress UndefinedConstant
				 */
				require_once ABSPATH . "wp-admin/includes/file.php";
			}

			$file_or_error = download_url(esc_url_raw($img_url));

			if (is_wp_error($file_or_error)) {
				return new \WP_Error("biblia-services-internal-4", $internal_error_message, [
					"status" => 500
				]);
			}

			$upload_dir = wp_upload_dir();
			$image_type = exif_imagetype($file_or_error);
			$filename = $isbn . "." . image_type_to_extension($image_type);
			$absolute_filename = trailingslashit($upload_dir["path"]) . $filename;
			copy($file_or_error, $absolute_filename);
			@unlink($file_or_error);

			// 3. Create the WordPress media image post type

			$attach_id = wp_insert_attachment(
				[
					"guid" => trailingslashit($upload_dir["url"]) . $filename,
					"post_mime_type" => image_type_to_mime_type($image_type),
					"post_title" => sanitize_title($isbn),
					"post_content" => "",
					"post_status" => "inherit"
				],
				$absolute_filename,
				0
			);

			if (!$attach_id) {
				return new \WP_Error("biblia-services-internal-5", $internal_error_message, [
					"status" => 500
				]);
			}

			if (!function_exists("wp_crop_image")) {
				/**
				 * @psalm-suppress UnresolvableInclude
				 * @psalm-suppress UndefinedConstant
				 */
				require_once ABSPATH . "wp-admin/includes/image.php";
			}

			$attach_data = wp_generate_attachment_metadata($attach_id, $absolute_filename);
			wp_update_attachment_metadata($attach_id, $attach_data);

			return new \WP_REST_Response(["id" => $attach_id]);
		}
	}

	public function new_cover_permissions(\WP_REST_Request $request): bool {
		return is_user_logged_in();
	}
}
