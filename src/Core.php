<?php

namespace Webnestors\BibliaServices;

use stdClass;

final class Core {
	/** @var ?self */
	private static $instance = null;
	private string $basename;
	private string $root_file;
	private Settings $settings;

	private function __construct(string $root_file) {
		$this->basename = plugin_basename($root_file);
		$this->root_file = $root_file;
		$this->settings = new Settings();
	}

	/** @return self */
	static function instance(string $root_file) {
		if (!self::$instance) {
			self::$instance = new self($root_file);
		}

		return self::$instance;
	}

	function init(): void {
		if (is_admin()) {
			add_action("plugin_action_links_{$this->basename}", [$this, "action_links"]);
			add_action("admin_menu", [$this->settings, "register_menu"]);
			add_action("admin_init", [$this->settings, "register_settings"]);
		}

		add_action("init", [$this, "on_init"]);
		add_shortcode(
			"biblia-services:search",
			new View("search", ["biblia-services-js"], ["biblia-services-css"])
		);

		$api_controller = new API_Controller($this->settings);
		add_action("rest_api_init", [$api_controller, "register_routes"]);
	}

	function on_init(): void {
		wp_register_script(
			"biblia-services-js",
			plugins_url("/", $this->root_file) . "biblia-services.js",
			["wp-api"],
			"0.1.0",
			true
		);

		wp_register_style(
			"biblia-services-css",
			plugins_url("/", $this->root_file) . "biblia-services.css",
			[],
			"0.1.0"
		);
	}

	function action_links(array $actions): array {
		$actions = array_merge(
			[
				sprintf(
					'<a href="%s">' . __("Settings") . "</a>",
					esc_url(admin_url("options-general.php?page=biblia-services"))
				)
			],
			$actions
		);

		return $actions;
	}
}
